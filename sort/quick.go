package sort

func QuickSort(arrInt []int, firstIndex int, lastIndex int) {
	if firstIndex >= lastIndex {
		return
	}

	// Граничный индекс, где начинается диапазона больших чисел.
	boundaryIndex := firstIndex

	for i := firstIndex; i < lastIndex; i++ {
		// arrInt[lastIndex] - Опорный эелемент с которым производится сравнение.
		if arrInt[i] <= arrInt[lastIndex] {
			arrInt[i], arrInt[boundaryIndex] = arrInt[boundaryIndex], arrInt[i]
			boundaryIndex++
		}
	}

	arrInt[lastIndex], arrInt[boundaryIndex] = arrInt[boundaryIndex], arrInt[lastIndex]

	QuickSort(arrInt, firstIndex, boundaryIndex-1)
	QuickSort(arrInt, boundaryIndex+1, lastIndex)
}
