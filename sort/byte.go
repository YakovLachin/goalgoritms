package sort

// По разрядная сортировка массива слов
// O(n)
func ByteSort(strings []string, length int) []string {
	byteArr := make([][]byte, length, length)

	maxLength := 0
	// определяем максимальную длину слова
	for i := 0; i < length; i++ {
		byteArr[i] = []byte(strings[i])
		lenString := len(strings[i])
		if maxLength < lenString {
			maxLength = lenString
		}

	}

	// реализуем сортировку подсчетом на каждый байт слова начиная с последнего
	// байта для каждого слова
	for i := maxLength - 1; i > -1; i-- {
		curRng := 0
		// определяем диапазон байтов для текущей позиции
		// по каждому слову
		for _, val := range byteArr {
			// если текущая позиция больше чем длина слова
			// значит в слове с текущей позицией нет байтов
			// и оно пропускается
			if i >= len(val) {

				continue
			}

			// маскимальый байт и есть тот диапазон, без учета нуля
			if curRng < int(val[i]) {
				curRng = int(val[i])
			}
		}

		ByteCountSort(&byteArr, length, curRng+1, i)
	}

	res := make([]string, length, length)
	for i := 0; i < length; i++ {
		res[i] = string(byteArr[i])
	}

	return res
}

// Сортирует массив байтов сортировкой подсчета по требуемой позиции
func ByteCountSort(arr *[][]byte, length int, rng int, curByte int) {
	equal := make([]int, rng, rng)
	curArr := *arr
	for i := 0; i < length; i++ {
		// если позиция больше длинны слова,
		// значит данного байта в слове нет и это слово
		// по умолчанию вперёд.
		if curByte >= len(curArr[i]) {
			equal[0]++
			continue
		}

		key := int(curArr[i][curByte])
		equal[key]++
	}

	less := make([]int, rng, rng)
	for i := 1; i < rng; i++ {
		less[i] = less[i-1] + equal[i-1]
	}

	res := make([][]byte, length, length)
	for i := 0; i < length; i++ {
		key := 0
		// Вспоминаем, что если
		// слово короче текущего curByte
		// по умолчанию слова ключ 0
		if curByte < len(curArr[i]) {
			key = int(curArr[i][curByte])
		}
		index := less[key]
		res[index] = curArr[i]
		less[key]++
	}

	*arr = res
}
