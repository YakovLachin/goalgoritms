package sort

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMergeSort_InputUnsortedArr_ReturnSorted(t *testing.T) {
	arr := []int{5, 3, 1, 2, 4, 4, 5, 6, 8, 5, 4, 3, 33, 45, 232}
	expected := []int{1, 2, 3, 3, 4, 4, 4, 5, 5, 5, 6, 8, 33, 45, 232}

	MergeSort(arr, 0, len(arr)-1, 9999)
	assert.Equal(t, expected, arr)
}
