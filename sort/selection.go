package sort



// O[n^2]
func SelectionSortViaFunc(arrInt []int, len int) []int {
	for i := 0; i < len -1; i++ {
		smallestKey := keyOfSmallestElement(arrInt, i, len - 1)
		arrInt[i], arrInt[smallestKey] = arrInt[smallestKey], arrInt[i]
	}

	return arrInt
}

func keyOfSmallestElement(arrInt []int, offset int, limit int) int{
	smallestKey := offset
	for i:= offset; i <= limit; i++ {
		if arrInt[i] < arrInt[smallestKey] {
			smallestKey = i
		}
	}

	return smallestKey
}

// Сортировка выбором O[n^2]
func SelectionSort(arrInt []int, length int) []int {
	for i := 0; i < length-1; i++ {
		smallestKey := i
		// определяем что
		for k:= i + 1; k <= length- 1; k++ {
			if arrInt[k] < arrInt[smallestKey] {
				smallestKey = k
			}
		}
		arrInt[i], arrInt[smallestKey] = arrInt[smallestKey], arrInt[i]
	}

	return arrInt
}
