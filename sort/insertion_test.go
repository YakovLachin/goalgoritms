package sort

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInsertionSort_InputUnsortedArr_ReturnSorted(t *testing.T) {
	arr := []int{3, 5, 1, 2, 4}
	expected := []int{1, 2, 3, 4, 5}

	res := InsertionSort(arr, len(arr))
	assert.Equal(t, expected, res)
}
