package sort

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCountKeysEqual(t *testing.T) {
	arr := []int{4, 1, 5, 0, 1, 6, 5, 1, 5, 3}
	expected := []int{1, 3, 0, 1, 1, 3, 1}
	rng := 7

	actual := CountKeysEqual(arr, len(arr), rng)

	assert.Equal(t, expected, actual)
}

func TestCountKeysLess(t *testing.T) {
	arr := []int{1, 3, 0, 1, 1, 3, 1}
	expected := []int{0, 1, 4, 4, 5, 6, 9}

	actual := CountKeysLess(arr, len(arr))

	assert.Equal(t, expected, actual)
}

func TestRearrange(t *testing.T) {
	arr := []int{4, 1, 5, 0, 1, 6, 5, 1, 5, 3}
	les := []int{0, 1, 4, 4, 5, 6, 9}
	rng := 7
	act := Rearrange(arr, les, len(arr), rng)
	exp := []int{0, 1, 1, 1, 3, 4, 5, 5, 5, 6}

	assert.Equal(t, exp, act)
}

func TestCountSort(t *testing.T) {
	arr := []int{4, 1, 5, 0, 1, 6, 5, 1, 5, 3}
	rng := 7
	exp := []int{0, 1, 1, 1, 3, 4, 5, 5, 5, 6}

	act := CountSort(arr, len(arr), rng)
	assert.Equal(t, exp, act)
}
