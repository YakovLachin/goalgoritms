package sort

func CountKeysEqual(arrInt []int, length int, rng int) []int {
	equal := make([]int, rng, rng)

	for i := 0; i < length; i++ {
		equal[arrInt[i]]++
	}

	return equal
}

func CountKeysLess(equal []int, length int) []int {
	less := make([]int, length, length)

	for i := 1; i < length; i++ {
		less[i] = equal[i-1] + less[i-1]
	}

	return less
}

func Rearrange(arr, less []int, len, rng int) []int {
	res := make([]int, len, len)
	for i := 0; i < len; i++ {
		key := arr[i]
		index := less[key]
		res[index] = arr[i]
		less[key]++
	}

	return res
}

func CountSort(arr []int, len, rng int) []int {
	equal := make([]int, rng, rng)
	for i := 0; i < len; i++ {
		equal[arr[i]]++
	}

	less := make([]int, rng, rng)
	for i := 1; i < rng; i++ {
		less[i] = less[i-1] + equal[i-1]
	}

	res := make([]int, len, len)

	for i := 0; i < len; i++ {
		key := arr[i]
		index := less[key]

		res[index] = arr[i]
		less[key]++
	}

	return res
}
