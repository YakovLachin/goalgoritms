package sort

// сортировка подсчетом для {x}, где x в диапазоне {1, 2}
func ReallySimpleSort(arrInt []int, len int) []int {
	k := 0
	for i := 0; i < len; i++ {
		if arrInt[i] == 1 {
			k++
		}
	}

	for i := 0; i < k+1; i++ {
		arrInt[i] = 1
	}

	for i := k; i < len; i++ {
		arrInt[i] = 2
	}

	return arrInt
}
