package sort

// Сортировка вставкой.
func InsertionSort(arrInt []int, len int) []int {
	for i := 1; i < len; i++ {
		j := i
		for k := i - 1; k > -1 && arrInt[k] > arrInt[j]; k-- {
			arrInt[k], arrInt[j] = arrInt[j], arrInt[k]
			j--
		}
	}

	return arrInt
}
