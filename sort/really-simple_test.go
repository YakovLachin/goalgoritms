package sort

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReallySimpleSort(t *testing.T) {
	arr := []int{2, 1, 2, 1, 1, 1, 2, 2, 1, 1, 2, 1, 2}
	expected := []int{1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2}

	assert.Equal(t, expected, ReallySimpleSort(arr, len(arr)))
}
