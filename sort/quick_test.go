package sort

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestQuickSort_InputUnsortedArr_ReturnSorted(t *testing.T) {
	arr := []int{5, 3, 1, 2, 4}
	expected := []int{1, 2, 3, 4, 5}

	QuickSort(arr, 0, len(arr)-1)
	assert.Equal(t, expected, arr)
}
