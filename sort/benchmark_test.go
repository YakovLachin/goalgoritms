package sort

import (
	"testing"
)

var arr = []int{5, 3, 1, 2, 45, 2325, 3, 1, 2, 4, 4, 5, 6, 4, 4, 5, 6, 8, 5, 4, 3, 33, 45, 2325, 3, 1, 2, 4, 4, 5, 6, 8, 5, 4, 3, 33, 8, 5, 4, 3, 33, 45, 2325, 3, 1, 2, 4, 4, 4, 5, 6, 8, 5, 4, 3, 33, 45, 2325, 3, 1, 2, 4, 4, 5, 6, 8, 5, 4, 3, 33, 4, 5, 6, 8, 5, 4, 3, 33, 45, 2325, 3, 1, 2, 4, 4, 5, 6, 8, 5, 4, 3, 33, 45, 2325, 3, 1, 2, 4, 4, 5, 6, 8, 5, 4, 3, 33, 2, 4, 4, 5, 6, 8, 5, 4, 3, 33, 45, 2325, 3, 1, 4, 4, 5, 6, 8, 5, 4, 3, 45, 2325, 3, 133, 45, 2325, 3, 1, 2, 4, 4, 5, 6, 8, 5, 4, 3, 33, 2, 4, 4, 5, 6, 8, 5, 4, 3, 33, 45, 2325, 3, 1, 2, 4, 4, 5, 6, 8, 5, 4, 3, 33, 45, 2325, 3, 1, 2, 4, 4, 5, 6, 8, 5, 4, 3, 33, 45, 2325, 3, 1, 2, 4, 4, 5, 6, 8, 5, 4, 3, 33, 45, 2325, 3, 1, 2, 4, 4, 5, 6, 8, 5, 4, 3, 33, 45, 2325, 3, 1, 2, 4, 4, 5, 6, 8, 5, 4, 3, 33, 45, 232}
var length = len(arr)
var rng = 2326

func BenchmarkSelectionSort(b *testing.B) {
	for i := 0; i < b.N; i++ {
		SelectionSort(arr, length)
	}
}

func BenchmarkInsertionSort(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertionSort(arr, length)
	}
}

func BenchmarkMergeSort(b *testing.B) {
	for i := 0; i < b.N; i++ {
		MergeSort(arr, 0, length-1, 3000)
	}
}

func BenchmarkQuickSort(b *testing.B) {
	for i := 0; i < b.N; i++ {
		QuickSort(arr, 0, length-1)
	}
}

func BenchmarkCountSort(b *testing.B) {
	for i := 0; i < b.N; i++ {
		CountSort(arr, length, rng)
	}
}
