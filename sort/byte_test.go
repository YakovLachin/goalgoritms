package sort

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestByteSort_InputUnsortedArr_ReturnSorted(t *testing.T) {
	arr := []string{
		"Yakov",
		"Vasiliy",
		"Dmitry",
		"Maxim",
		"Alexey",
		"Anton",
		"Alexander",
		"Ilya",
		"Andrey",
		"Olga",
		"Marina",
		"Ekaterina",
		"Konstantin",
	}

	exp := []string{
		"Alexander",
		"Alexey",
		"Andrey",
		"Anton",
		"Dmitry",
		"Ekaterina",
		"Ilya",
		"Konstantin",
		"Marina",
		"Maxim",
		"Olga",
		"Vasiliy",
		"Yakov",
	}

	act := ByteSort(arr, len(arr))
	assert.Equal(t, exp, act)
}
