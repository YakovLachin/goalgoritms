package sort

func MergeSort(arrInt []int, firstIndex int, lastIndex int, inf int) {
	if firstIndex == lastIndex {
		return
	}

	middleIndex := firstIndex + (lastIndex-firstIndex)/2
	MergeSort(arrInt, firstIndex, middleIndex, inf)
	MergeSort(arrInt, middleIndex+1, lastIndex, inf)
	subArrLeft := make([]int, middleIndex-firstIndex+1, middleIndex-firstIndex+2)
	copy(subArrLeft, arrInt[firstIndex:middleIndex+1])
	subArrLeft = append(subArrLeft, inf)
	subArrRight := make([]int, lastIndex-middleIndex, lastIndex-middleIndex+2)
	copy(subArrRight, arrInt[middleIndex+1:lastIndex+1])
	subArrRight = append(subArrRight, inf)

	i := 0
	j := 0

	for k := firstIndex; k <= lastIndex; k++ {
		if subArrLeft[i] < subArrRight[j] {
			arrInt[k] = subArrLeft[i]
			i++
		} else {
			arrInt[k] = subArrRight[j]
			j++
		}
	}
}
