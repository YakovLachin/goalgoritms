package removeDuplicate

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func Test_RemoveDuplicate(t *testing.T) {
	arr := []int{1, 2, 2, 3, 4, 4, 5, 6, 6}
	expected := []int{1, 2, 3, 4, 5, 6}
	actual := RemoveDuplicate(arr)
	assert.Equal(t, expected, actual)
}

func Test_NewSliceWithoutDuplicate(t *testing.T) {
	arr := []int{1, 2, 2, 3, 4, 4, 5, 6, 6}
	expected := []int{1, 2, 3, 4, 5, 6}
	actual := NewSliceWithoutDuplicate(arr)
	assert.Equal(t, expected, actual)
}

func Test_RemoveDuplicateViaMap(t *testing.T) {
	arr := []int{1, 2, 2, 3, 4, 4, 5, 6, 6}
	expected := []int{1, 2, 3, 4, 5, 6}
	actual := RemoveDuplicate(arr)
	assert.Equal(t, expected, actual)
}

var arr = []int{1, 2, 2, 213, 1233 ,1, 3323, 33, 33, 1, 2, 2, 213, 1233}

func BenchmarkRemoveDuplicateViaFunc(b *testing.B) {
	for i := 0; i < b.N; i++ {
		NewSliceWithoutDuplicateViaFunc(arr)
	}
}

func BenchmarkNewSliceWithoutDuplicate(b *testing.B) {
	for i := 0; i < b.N; i++ {
		NewSliceWithoutDuplicate(arr)
	}
}

func BenchmarkRemoveDuplicate(b *testing.B) {
	for i := 0; i < b.N; i++ {
		RemoveDuplicate(arr)
	}
}

func BenchmarkRemoveDuplicateViaMap(b *testing.B) {
	for i := 0; i < b.N; i++ {
		RemoveDuplicateViaMap(arr)
	}
}

func BenchmarkRemoveDuplicates(b *testing.B) {
	for i := 0; i < b.N; i++ {
		RemoveDublicates(arr)
	}
}
