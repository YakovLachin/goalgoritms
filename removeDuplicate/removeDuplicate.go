package removeDuplicate

import (
	"gitlab.com/YakovLachin/goalgoritms/search"
)


func NewSliceWithoutDuplicateViaFunc(arrInt []int) []int {
	var res   []int
	length := len(arrInt)
	for i := 0; i < length; i++ {
		_, err := search.LinearSearch(arrInt[i + 1:], len(arrInt[i + 1:]), arrInt[i])
		if err != nil {
			res = append(res, arrInt[i])
		}
	}

	return  res
}

// Удаляет дубликат, посредством создания нового слайса
// не записывая повторные значения
// O (n^2)
func NewSliceWithoutDuplicate(arrInt []int) []int {
	var res []int
	length := len(arrInt)
	for i := 0; i < length; i++ {
		exist := false
		for k := i + 1; k < length; k++ {
			if arrInt[k] == arrInt[i] {
				exist = true
				break
			}
		}

		if exist == false {
			res = append(res, arrInt[i])
		}
	}

	return  res
}

// Удаляет из массива повторяющиеся элементы
// O (n^2)
func RemoveDuplicate(arrInt []int) []int {
	length := len(arrInt)
	for i := 0; i < length; i++ {
		for k := i + 1; k < length; k++ {
			if arrInt[k] == arrInt[i] {
				arrInt = append(arrInt[:k], arrInt[k+1:]...)
				k--
				length--
			}
		}
	}

	return  arrInt
}

// Удаляет из массива элементы использу Map как хранилище
// Сама операция добавления в мап весьма тяжелая.
// O(n)
func RemoveDuplicateViaMap(arrInt []int) []int {
	resMap := make(map[int]bool)
	length := len(arrInt)
	for i := 0; i < length; i++ {
		val := arrInt[i]
		if !resMap[val] {
			arrInt = append(arrInt[:i], arrInt[i+1:]...)
			resMap[val] = true
			length--
		}
	}

	return  arrInt
}

func RemoveDublicates(phones []int) []int {
    var dublicates []int

    length := len(phones)

    if length < 2 {
        return phones
    }

    var result []int
    for i := 0; i < length; i++ {
        var skip bool
        for _, dublicate := range dublicates {
            if i == dublicate {
                skip = true
                break
            }
        }

        if skip {
            continue
        }

        for k := i + 1; k < length; k++ {
            if phones[i] == phones[k] {
                dublicates = append(dublicates, k)
            }
        }

        result = append(result, phones[i])
    }

    return result
}
