package search

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestLinearSearch_InputArrWithReq_ReturnReq(t *testing.T) {
	arr := []int{1, 2, 3, 4, 5}

	res, err := LinearSearch(arr, len(arr), 5)
	assert.Equal(t, 5, res)
	assert.Nil(t, err)
}

func TestLinearSearch_InputArrWithoutReq_ThrowErr(t *testing.T) {
	arr := []int{1, 2, 3, 4, 5}

	res, err := LinearSearch(arr, len(arr), 6)
	assert.Equal(t, 0, res)
	assert.NotNil(t, err)
}

func TestSentinelLinearSearch_InputArrWithReq_CapacityNotChanged(t *testing.T) {
	arr := []int{1, 2, 3, 4, 5}

	originalCap := cap(arr)
	_, err := LinearSearch(arr, len(arr), 6)
	assert.Equal(t, originalCap, cap(arr))
	assert.NotNil(t, err)
}

func TestSentinelLinearSearch_InputArrWithReq_ReturnReq(t *testing.T) {
	arr := []int{1, 2, 3, 4, 5}

	res, err := SentinelLinearSearch(arr, len(arr), 5)
	assert.Equal(t, 5, res)
	assert.Nil(t, err)
}

func TestSentinelLinearSearch_InputArrWithoutReq_ThrowErr(t *testing.T) {
	arr := []int{1, 2, 3, 4, 5}

	res, err := SentinelLinearSearch(arr, len(arr), 6)
	assert.Equal(t, 0, res)
	assert.NotNil(t, err)
}

func TestLinearSearch_InputArrWithReq_CapacityNotChanged(t *testing.T) {
	arr := []int{1, 2, 3, 4, 5}

	originalCap := cap(arr)
	_, err := SentinelLinearSearch(arr, len(arr), 6)
	assert.Equal(t, originalCap, cap(arr))
	assert.NotNil(t, err)
}

var arr = []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60}
var req = 30
func BenchmarkLinearSearch(b *testing.B) {
	for i := 0; i < b.N; i++ {
		LinearSearch(arr, len(arr), req)
	}
}

func BenchmarkSentinelLinearSearch(b *testing.B) {
	for i := 0; i < b.N; i++ {
		SentinelLinearSearch(arr, len(arr), req)
	}
}

func BenchmarkBinarySearc2h(b *testing.B) {
	for i := 0; i < b.N; i++ {
		BinarySearch(arr, len(arr), req)
	}
}
