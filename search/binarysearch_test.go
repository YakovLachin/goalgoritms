package search

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestBinarySearch_InputArrWithReq_ReturnReq(t *testing.T) {
	arr := []int{1, 2, 3, 4, 5}

	res, err := BinarySearch(arr, len(arr), 5)
	assert.Equal(t, 5, res)
	assert.Nil(t, err)
}

func TestBinarySearch_InputArrWithoutReq_ThrowErr(t *testing.T) {
	arr := []int{1, 2, 3, 4, 5}

	res, err := BinarySearch(arr, len(arr), 6)
	assert.Equal(t, 0, res)
	assert.NotNil(t, err)
}

func TestBinarySearch_InputArrWithReq_CapacityNotChanged(t *testing.T) {
	arr := []int{1, 2, 3, 4, 5}

	originalCap := cap(arr)
	_, err := BinarySearch(arr, len(arr), 6)
	assert.Equal(t, originalCap, cap(arr))
	assert.NotNil(t, err)
}

func BenchmarkBinarySearch(b *testing.B) {
	for i := 0; i < b.N; i++ {
		BinarySearch(arr, len(arr), req)
	}
}
