package search

import "fmt"

// Бинарный поиск
// Время работы алгоритма O(log n)
func BinarySearch(arrInt []int, len int, req int) (int, error) {
	begin := 0
	end := len
	for begin < end {
		queried := begin + (end - begin) / 2
		if arrInt[queried] == req {
			return arrInt[queried], nil
		} else if arrInt[queried] > req {
			end = queried
		} else {
			begin = queried + 1
		}
	}

	return 0, fmt.Errorf("Not Found")
}
