package search

import "fmt"

// Линейный поиск.
// Время работы алгоритма O(n)
func LinearSearch(arrInt []int, arrLen int, req int) (int, error) {
	for i := 0; i < arrLen; i++ {
		if arrInt[i] == req {
			return arrInt[i], nil
		}
	}

	return 0, fmt.Errorf("element Not Found")
}

// Линейный поиск. Боллее быстрый для большего количества элементов в массиве
// Время работы алгоритма O(n)
func SentinelLinearSearch(arrInt []int, arrLen int, req int) (int, error) {
	arrLen = arrLen - 1
	last := arrInt[arrLen]
	arrInt[arrLen] = req
	i := 0

	for arrInt[i] != req {
		i++
	}

	arrInt[arrLen] = last
	if i < arrLen || last == req {
		return arrInt[i], nil
	}

	return 0, fmt.Errorf("element Not Found")
}
