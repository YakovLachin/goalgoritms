package parallel

import "testing"

func TestRunWithMutexForMultiplie(t *testing.T) {
	in :=1
	actual := RunWithMutex(in, 1, 5000)
	if actual != 5001 {
		t.Fail()
	}
}
