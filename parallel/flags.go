package parallel

import (
	"fmt"
	"runtime"
)

func RunWithBarries(in []int, increaseVal, numWorkers int) []int {
	var contin = make([]int, numWorkers)
	var done = make([]int, numWorkers)
	for i := 0; i < numWorkers; i++ {
		go func(i int) {
				in[i] += increaseVal
				done[i] = 1
			fmt.Println(i, " done")

			for contin[i] == 0 {
					runtime.Gosched()
					continue
			}

			fmt.Println(i, " exit")
		}(i)
	}

	var d = make(chan struct{})

	go func() {
			for i := 0; i < numWorkers; i++ {
				for done[i] == 0 {
					runtime.Gosched()
				}
				done[i] = 0
				contin[i] = 1
			}
		d <- struct{}{}
	}()

	<-d

	return in
}

func RunWithFlags(in []int, increaseVal, num, numWorkers int) []int {
	var contin = make([]int, numWorkers)
	var done = make([]int, numWorkers)
	for i := 0; i < numWorkers; i++ {
		go func(i int) {
			for n := 0; n < num; n++ {
				in[i] += increaseVal
				done[i] = 1
				for contin[i] == 0 || done[i] == 1 {
					runtime.Gosched()
					continue
				}
			}
		}(i)
	}

	var d = make(chan struct{})
	go func() {
		for n:=0; n < num; n++ {
			for i := 0; i < numWorkers; i++ {
				for done[i] == 0 {
					runtime.Gosched()
					continue
				}
				done[i] = 0
				contin[i] = 1
			}
		}

		d <- struct{}{}
	}()

	<-d

	return in
}
