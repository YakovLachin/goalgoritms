package parallel

import "testing"

func TestRunTicketAlgorithm(t *testing.T) {
	in :=1
	actual := RunTicketAlgorithm(in, 1, 200)
	if actual != 201 {
		t.Fail()
	}
}
