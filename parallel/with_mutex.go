package parallel

import "sync"

type Int struct {
	val int
	counter int
	sync.Mutex
}

func RunWithMutex(in, increaseValue, num int) int {
	inSync := &Int{
		val: in,
	}

	for i :=0; i < num; i++ {
		go func() {
			inSync.Lock()
			inSync.val +=increaseValue
			inSync.counter +=1
			inSync.Unlock()
		}()
	}

	for inSync.counter < num {}

	return  inSync.val
}
