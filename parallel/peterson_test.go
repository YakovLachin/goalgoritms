package parallel

import (
	"fmt"
	"testing"
)

func TestRunPeterson(t *testing.T) {
	in :=1
	actual := RunPeterson(in, 1)
	if actual != 3 {
		t.Fail()
	}
}

func TestRunPetersonForMultiplie(t *testing.T) {
	in :=1
	actual := RunPetersonForN(in, 1, 5000)
	fmt.Println(actual)
	if actual != 5001 {
		t.Fail()
	}
}
