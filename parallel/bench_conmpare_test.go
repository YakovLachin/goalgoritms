package parallel

import "testing"

func BenchmarkRunPetersonForN(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	for n := 0; n < 1; n++ {
		RunPetersonForN(1, 1, 5000)
	}
}

func BenchmarkRunWithMutexForN(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	for n := 0; n < 1; n++ {
		RunWithMutex(1, 1, 5000)
	}
}

func BenchmarkRunTicketAlgorithm(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	for n := 0; n < 1; n++ {
		RunTicketAlgorithm(1, 1, 5000)
	}
}
