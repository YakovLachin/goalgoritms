package parallel

import (
	"fmt"
	"runtime"
)

func RunTicketAlgorithm(in, increaseValue, num int) int {
	var counter int
	for i := 0; i < num; i++ {
		// протокол входа
		go func(ticketNum int) {
			for counter < ticketNum {
				fmt.Println(ticketNum, " is waiting")
				runtime.Gosched()
				continue
			}
			fmt.Println(ticketNum, " done")
			in += increaseValue
			counter +=1
		}(i)
	}

	for counter < num {
		continue
	}

	return in
}
