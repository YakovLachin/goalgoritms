package parallel

import (
	"fmt"
	"testing"
)


func TestRunWithFlags (t *testing.T) {
	in := make([]int, 100)
	actual := RunWithFlags(in, 1, 100, 100)
	fmt.Println(actual)
}

func TestRunWithLinearBarrier (t *testing.T) {
	in := make([]int, 10000)
	actual := RunWithBarries(in, 1, 10000)
	fmt.Println(actual)
}
