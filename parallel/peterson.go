package parallel

import (
	"runtime"
	"time"
)

func RunPeterson(in, increaseValue int) int {
	Ins := []bool {false, false, false}
	var last int
	var counter int

	p1 := func (){
		// протокол входа
		last=1
		Ins[1] = true
		// Если он был последний и другой процесс прошел
		for Ins[2] && last == 1 {
		}
		// критическая секция
		in +=increaseValue
		counter +=1

		// протокол выхода
		Ins[1]= false
		// не критическая секция
	}

	p2 := func (){
		// Этот Поток будет утверждать, что он последний
		last=2
		Ins[2] = true
		// Если он был последний и другой процесс прошел
		for Ins[1] && last == 2 {
		}
		// критическая секция
		in +=increaseValue
		counter +=1

		// протокол выхода
		Ins[2]= false
	}

	go p1()
	go p2()

	time.Sleep(time.Millisecond)

	return in
}

func RunPetersonForN(obj, increaseValue, n int) int {
	last := make([]int, n)
	in := make([]int, n)
	counter := 0

	for i:=0; i < n; i++  {
		go func(i int) {
			// старт стадий
			for j:=0; j < n; j++ {
				// Говорю, что на этой стадии последний Я был
				last[j] = i

				// Моя стадия
				in[i] = j

				// старт проверок стадий для каждого потока к
				for k:= 0; k < n && i != k; k++{
					// если стадия потока k больше или равна моей (надо ему дать доработать)
					// и на этой стадии последний я, то я жду
					for in[k] >= in[i] && last[j] == i {
						runtime.Gosched()
						continue
					}
				}
			}

			// критическая секция
			obj += increaseValue
			counter +=1

			// протокол выхода
			in[i] = -1
			// не критическая секция

		}(i)
	}

	for counter < n  {
		runtime.Gosched()
		continue
	}

	return obj
}
